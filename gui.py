import urllib.parse
import urllib.request
import tkinter as gui
import tkinter.messagebox


class Application(gui.Frame):
    def __init__(self, master=None):
        gui.Frame.__init__(self, master, padx=10, pady=10)
        self.pack()
        self.build_gui()

    def build_gui(self):
        __carrier_list = ['AT&T', 'Verizon', 'Sprint', 'T-Mobile', 'MetroPCS']
        self.carrier = gui.StringVar(self)
        self.carrier.set(__carrier_list[0])
        self.phone_number = gui.StringVar(self, '')
        self.phone_number.trace('w', lambda name, index, mode, n=self.phone_number: self.validate_n_len(n))
        self.phone_number.trace('w', lambda name, index, mode, n=self.phone_number: self.validate_n_type(n))
        self.message = gui.StringVar(self, '')
        self.message.trace('w', lambda name, index, mode, m=self.message: self.validate_m_len(m))

        cl = gui.Label(self, text='Carrier')
        cl.bind('<Button>', lambda e: self.carriers.focus())
        cl.grid(row=0, sticky=gui.W, padx=(0, 20), pady=(0, 20))

        pl = gui.Label(self, text='Phone #')
        pl.bind('<Button>', lambda e: self.phone_number_entry.focus_set())
        pl.grid(row=1, sticky=gui.W, padx=(0, 20), pady=(0, 20))

        ml = gui.Label(self, text='Message')
        ml.bind('<Button>', lambda e: self.message_box.focus())
        ml.grid(row=2, sticky=gui.W, padx=(0, 20), pady=(0, 20))

        self.carriers = gui.OptionMenu(self, self.carrier, *__carrier_list)
        self.carriers['width'] = 10
        self.carriers.grid(row=0, column=1, sticky=gui.E + gui.W, pady=(0, 20))

        self.phone_number_entry = gui.Entry(self, textvariable=self.phone_number, width=40)
        self.phone_number_entry.grid(row=1, column=1, sticky=gui.E + gui.W, pady=(0, 20))

        self.message_box = gui.Entry(self, textvariable=self.message, width=40)
        self.message_box.grid(row=2, column=1, pady=(0, 20))

        self.counter = gui.Message(self, text="0")
        self.counter.grid(row=3, column=1, sticky=gui.E, pady=(0, 20))

        self.send_button = gui.Button(self, text='Send', command=lambda: self.handle_send(), bg='green')
        self.send_button.grid(row=4, column=1, sticky=gui.E + gui.W, pady=(0, 20))

    def validate_n_len(self, n):
        if len(n.get()) > 10:
            n.set(n.get()[:10])

    def validate_n_type(self, n):
        if not str.isdigit(n.get()):
            self.show_prompt('error', 'Number only!')
            n.set(n.get()[:-1])

    def validate_m_len(self, m):
        self.counter['text'] = str(len(m.get()))
        if len(m.get()) > 160:
            m.set(m.get()[:160])

    def show_prompt(self, kind, message):
        if kind == 'sucess':
            tkinter.messagebox.showinfo("Success", message)
            return
        elif kind == 'warning':
            tkinter.messagebox.showwarning("Warning", message)
            return
        elif kind == 'error':
            tkinter.messagebox.showerror("Error", message)
        else:
            tkinter.messagebox.showinfo("Hey!", message)

    def handle_send(self):
        if self.send_sms() == 200:
            self.show_prompt('success', 'Message sent!')
            return
        self.show_prompt('error', 'Something went wrong!')

    def request_valid(self):
        carrier = self.carrier != None
        number = len(self.phone_number.get()) == 10 and str(self.phone_number).isdigit()
        message = len(self.message.get()) <= 160 and len(self.message.get()) != 0

        if carrier and number and message:
            return True
        return False

    def send_sms(self):
        try:
            carrier = self.carrier.get()
            phone_number = self.phone_number.get()
            formatted_number = ''

            if carrier == 'AT&T':
                formatted_number = phone_number + "@txt.att.net"
            elif carrier == 'Verizon':
                formatted_number = phone_number + "@vtext.com"
            elif carrier == 'Sprint':
                formatted_number = phone_number + "@messaging.sprintpcs.com"
            elif carrier == 'T-Mobile':
                formatted_number = phone_number + "@tmomail.net"
            else:
                formatted_number = phone_number + "@mymetropcs.com"

            data = urllib.parse.urlencode({'to': formatted_number, 'from': 'FreeSMS <sms@tmrhmd.com>', 'subject': 'FreeSMS',
                                           'text': self.message_box.get()}).encode('utf-8')

            realm = urllib.request.HTTPPasswordMgrWithDefaultRealm()
            realm.add_password(None, 'https://api.mailgun.net/v3/sandbox12513.mailgun.org/messages', 'api', 'key-3d8u2qxln8c1uf0aki9re108vs7j9fs1')
            handler = urllib.request.HTTPBasicAuthHandler(realm)
            opener = urllib.request.build_opener(handler)
            urllib.request.install_opener(opener)

            response = urllib.request.urlopen('https://api.mailgun.net/v3/sandbox12513.mailgun.org/messages', data)
            return response.getcode()
        except Exception as e:
            pass


root = gui.Tk()
root.wm_title('Free SMS')
root.resizable(width=False, height=False)

app = Application(master=root)
app.mainloop()
