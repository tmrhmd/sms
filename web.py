#!/env/bin/python3

import os
import urllib.parse

import tornado.web
import tornado.gen
import tornado.httpclient
import tornado.httpserver
import tornado.ioloop

from tornado.options import define, options

define("port", default=3000, help="run on the given port", type=int)

client = tornado.httpclient.AsyncHTTPClient()


class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r"/", IndexHandler)
        ]
        settings = dict(
            template_path=os.path.join(os.path.dirname(__file__), "assets", "html"),
            static_path=os.path.join(os.path.dirname(__file__), "assets"),
            debug=True,
        )
        super(Application, self).__init__(handlers, **settings)


class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("index.html")

    def post(self):
        carrier = self.get_argument('carrier')
        phone_number = self.get_argument('phone_number')
        message = self.get_argument('message')

        if carrier == 'att':
            phone_number = phone_number + "@txt.att.net"
        elif carrier == 'verizon':
            phone_number = phone_number + "@vtext.com"
        elif carrier == 'sprint':
            phone_number = phone_number + "@messaging.sprintpcs.com"
        elif carrier == 'tmobile':
            phone_number = phone_number + "@tmomail.net"
        else:
            phone_number = phone_number + "@mymetropcs.com"

        data = urllib.parse.urlencode({'from': 'FreeSMS <sms@tmrhmd.com>', 'to': phone_number, 'subject': 'FreeSMS', 'text': message})
        request = tornado.httpclient.HTTPRequest("https://api.mailgun.net/v3/sandbox12513.mailgun.org/messages",
                                                 method='POST', auth_username="api",
                                                 auth_password="key-3d8u2qxln8c1uf0aki9re108vs7j9fs1", body=data)
        if self.send(request):
            self.set_status(200)
            self.finish('OK')
            return
        self.set_status(500)
        self.finish('ERROR')

    @tornado.gen.coroutine
    def send(self, request):
        response = yield client.fetch(request)
        if response.code == 200:
            return False
        return True


def main():
    tornado.options.parse_command_line()
    http_server = tornado.httpserver.HTTPServer(Application())
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.current().start()


if __name__ == "__main__":
    main()
