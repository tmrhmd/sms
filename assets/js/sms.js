$(document).ready(function () {
    var smsForm = $('.ui.form');
    var carriers = $('.ui.buttons .button');
    var carrier = $('#carrier');
    var phoneNumber = $('#phone_number');
    var message = $('#message');
    var counter = $('#counter');
    var response = $('.response');

    carriers.on('click', function () {
        $(this).addClass('active').siblings().removeClass('active');
        carrier.val($(this).attr('data-carrier'));
    });

    phoneNumber.mask('(000) 000-0000');

    message.on('change keyup paste', function () {
        var length = 160 - message.val().length;
        counter.text(length);
        counter.toggleClass('green', length >= 100).toggleClass('yellow', length >= 50 && length < 100).toggleClass('red', length < 50);
    });

    function submit() {
        var data = {
            'carrier': carrier.val(),
            'phone_number': phoneNumber.cleanVal(),
            'message': message.val()
        };
        $.ajax({
            type: "POST",
            url: window.location,
            data: data,
            success: function () {
                response.html('<i class="green checkmark icon"></i>Message sent!');
                smsForm.dimmer('show');
            },
            error: function () {
                response.html('<i class="red remove icon"></i>Something went wrong!');
                smsForm.dimmer('show');
            },
            dataType: "text"
        });
    }

    smsForm.form({
            phone_number: {
                identifier: 'phone_number',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Enter a valid phone number'
                    }
                ]
            },
            message: {
                identifier: 'message',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Enter a message'
                    },
                    {
                        type: 'maxLength[160]',
                        prompt: 'You can\'t send more than 160 characters'
                    }
                ]
            }
        }, {
            on: 'blur',
            onSuccess: function (e) {
                e.preventDefault();
                submit();
                smsForm.form('clear');
            }
        }
    )
});